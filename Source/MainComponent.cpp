/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    addAndMakeVisible (colourSelector1);
    
    addAndMakeVisible(callumButton1);
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    DBG ("Width: " << getWidth() << "Height: " << getHeight());
    
    //colourSelector1.setBounds(0, 0, getWidth(), getHeight()/2);
    
    callumButton1.setBounds(0, 0, getWidth(), getHeight());
}

void MainComponent::paint (Graphics& g)
{
    
}

void MainComponent::mouseEnter (const MouseEvent& event)
{
    DBG("mouse enter");
    
    colour = Colours::lightgreen;
    
    repaint();
}

void MainComponent::mouseExit (const MouseEvent& event)
{
    DBG("mouse exit");
    
    colour = Colours::green;
    
    repaint();
}

void MainComponent::mouseUp (const MouseEvent& event)
{
    DBG("mouse up");
    
    colour = Colours::forestgreen;
    
    repaint();
}

void MainComponent::mouseDown (const MouseEvent& event)
{
    DBG("mouse down");
    
    colour = Colours::darkgreen;
    
    repaint();
}
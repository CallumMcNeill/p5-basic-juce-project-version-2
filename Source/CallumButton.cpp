//
//  CallumButton.cpp
//  JuceBasicWindow
//
//  Created by Callum McNeill on 08/01/2018.
//
//

#include "CallumButton.hpp"

CallumButton::CallumButton()
{
    setSize (500, 400);
}

CallumButton::~CallumButton()
{
    
}

void CallumButton::paint (Graphics& g)
{
    g.setColour(Colours::green);
    
    g.fillEllipse(10, 10, 200, 200);
}

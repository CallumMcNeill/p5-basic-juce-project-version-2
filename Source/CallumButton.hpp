//
//  CallumButton.hpp
//  JuceBasicWindow
//
//  Created by Callum McNeill on 08/01/2018.
//
//

#ifndef CallumButton_hpp
#define CallumButton_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include <stdio.h>

//==============================================================================

class CallumButton : public Component
{
public:
    //==============================================================================
    CallumButton();
    ~CallumButton();
    
    void paint (Graphics& g) override;
    
private:
    
    //==============================================================================
};

#endif /* CallumButton_hpp */